require('dotenv').config({
  path: require('path').resolve(process.cwd(), '.env.dev'),
  debug: !!process.env.DEBUG,
});

require('module-alias').addAlias('app', __dirname);

import 'reflect-metadata';
import express from 'express';
import bootServer from 'app/boot';

import { ENV } from 'app/configs';
import getAppRouter from 'app/routes';
import { errorCatcher } from 'app/utils/errors';

const app = express();

bootServer(app)
  .then(async () => {
    app.use(getAppRouter());

    errorCatcher(app);

    app.listen({ port: ENV.PORT }, () => {
      try {
        const m = Math.ceil((process.memoryUsage()['heapUsed'] / 1024 / 1024 / 1024) * 100) / 100;
        console.log('Memory usage: ', m, 'GB');
      } catch (e) {
        console.log('Memory usage error: ', e.message);
      }
      console.log(`🚀 !!! Server ready at http://localhost:${ENV.PORT}/graphql`);
    });
  })
  .catch((error) => {
    console.log('Booting error! ', error);
  });
