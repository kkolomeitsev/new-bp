import { Application } from 'express';
import helmet from 'helmet';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import { ENV } from 'app/configs';
import { connect } from 'app/utils/db';
import sendHttpError from 'app/middlewares/sendHttpError';

export default async (app: Application) => {
  app.set('trust proxy', 1);

  const environmentType = ENV.ENV_TYPE === 'production' ? 'production' : 'development';

  await mongoose.connect(ENV.MONGODB_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const sessionConnection = await mongoose.createConnection(ENV.MONGODB_SESSIONS_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const connectMongoStore = connectMongo(session);

  app.locals.mongoStore = new connectMongoStore({
    mongooseConnection: sessionConnection,
    collection: 'sessions',
    stringify: false,
    serialize: (session: any) => {
      const obj: any = {};
      let prop;

      for (prop in session) {
        if (prop === 'cookie') {
          obj.cookie = session.cookie.toJSON ? session.cookie.toJSON() : session.cookie;
        } else {
          obj[prop] = session[prop];
        }
      }

      return obj;
    },
    unserialize: (x: any) => x,
  } as any);

  const dbConnection = await connect();

  if (!dbConnection) throw new Error('Error can not connect to database!');

  app.use(helmet());

  app.use(sendHttpError);

  app.use(bodyParser.urlencoded({ extended: false }));

  app.use(bodyParser.json());

  app.use(cookieParser());

  app.use(
    session({
      secret: 'dsyIYtgBAcrPvIkybymeLJiPWNG9vzxm',
      resave: false,
      saveUninitialized: true,
      name: 'x_sid',
      cookie: {
        path: '/',
        httpOnly: true,
        secure: environmentType === 'production' ? true : false,
        domain: `.${ENV.CORS_DOMAIN}`,
      },
      store: app.locals.mongoStore,
    })
  );
};
