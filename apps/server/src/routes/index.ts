/* eslint-disable @typescript-eslint/no-non-null-assertion */
import express from 'express';
import getGraphQLRouter from 'app/routes/graphql';

export default function getAppRouter() {
  const router = express.Router();

  router.use(getGraphQLRouter());

  return router;
}
