import './types';

export const schema = require('./corp').buildSchema();

export const prepareContext = require('./corp').prepareContext;
