import { schemaComposer } from 'app/schema/graphql-compose';
import { ProductOTC } from './Product';

export const ProductSettingFilterITC = schemaComposer.createInputTC({
  name: 'ProductSettingFilterInput',
  fields: {
    id: 'Int',
    title: 'String',
    description: 'String',
    kindType: 'String',
    value: 'String',
  },
});

export const ProductSettingITC = ProductSettingFilterITC.clone('ProductSettingInput')
  .removeField(['id'])
  .makeFieldNonNull(['title', 'value'])
  .addFields({
    product: 'ProductFilterInput!',
  });

export const ProductSettingUpdateITC = ProductSettingITC.clone(
  'ProductSettingUpdateInput'
).makeFieldNullable(['title', 'value', 'product']);

export const ProductSettingOTC = schemaComposer.createObjectTC({
  name: 'ProductSettingType',
  fields: {
    id: 'Int!',
    title: 'String!',
    description: 'String',
    kindType: 'String',
    value: 'String!',
    product: (): any => ProductOTC.getTypeNonNull(),
  },
});

export const ProductSettingListOTC = schemaComposer.createObjectTC({
  name: 'ProductSettingListType',
  fields: {
    page: 'Int!',
    perPage: 'Int!',
    count: 'Int!',
    items: (): any => [ProductSettingOTC],
  },
});
