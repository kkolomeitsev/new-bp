import { schemaComposer } from 'app/schema/graphql-compose';
import { ProductSettingOTC } from './ProductSetting';

export const ProductFilterITC = schemaComposer.createInputTC({
  name: 'ProductFilterInput',
  fields: {
    id: 'Int',
    title: 'String',
    description: 'String',
  },
});

export const ProductITC = ProductFilterITC.clone('ProductInput')
  .removeField(['id'])
  .makeFieldNonNull(['title']);

export const ProductUpdateITC = ProductITC.clone('ProductUpdateInput').makeFieldNullable(['title']);

export const ProductOTC = schemaComposer.createObjectTC({
  name: 'ProductType',
  fields: {
    id: 'Int!',
    title: 'String!',
    description: 'String',
    settings: (): any => [ProductSettingOTC],
  },
});

export const ProductListOTC = schemaComposer.createObjectTC({
  name: 'ProductListType',
  fields: {
    page: 'Int!',
    perPage: 'Int!',
    count: 'Int!',
    items: (): any => [ProductOTC],
  },
});
