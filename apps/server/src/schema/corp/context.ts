import express from 'express';
import { FieldNode } from 'graphql';
import DataLoader from 'dataloader';

export interface TContext {
  dataloaders: WeakMap<ReadonlyArray<FieldNode>, DataLoader<string, object>>;
  req: express.Request;
  res: express.Response;
}

export async function prepareContext({
  req,
  res,
}: {
  req: express.Request;
  res: express.Response;
}): Promise<TContext> {
  return {
    dataloaders: new WeakMap(),
    req,
    res,
  };
}
