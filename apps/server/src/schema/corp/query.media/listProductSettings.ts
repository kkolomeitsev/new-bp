import { FindManyOptions } from 'typeorm';
import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { ProductSetting } from 'app/entity/ProductSetting';
import { ProductSettingListOTC, ProductSettingFilterITC } from 'app/schema/types';

const listProductSettings: FieldConfig<{
  page: number;
  perPage: number;
  filter?: ProductSetting;
}> = {
  type: ProductSettingListOTC,
  args: {
    page: {
      type: 'Int',
      defaultValue: 1,
    },
    perPage: {
      type: 'Int',
      defaultValue: 100,
    },
    filter: ProductSettingFilterITC,
  },
  resolve: async (_, args, __, info) => {
    const productSettingRepository = getRepository<ProductSetting>(ProductSetting);

    if (!productSettingRepository) throw new Error('DB connection error!');

    const relations = getRelationsForField(info, 'items');

    const { page, perPage, filter } = args;

    const productSettingOptions: ProductSetting = filter
      ? JSON.parse(JSON.stringify(filter))
      : null;

    const findOptions: FindManyOptions<ProductSetting> = {
      relations,
      take: perPage,
      skip: (page - 1) * perPage,
    };

    if (productSettingOptions) {
      findOptions.where = productSettingOptions;
    }

    const productSettings = await productSettingRepository.findAndCount(findOptions);

    const listData = {
      page,
      perPage,
      items: productSettings[0] || [],
      count: productSettings[1] || 0,
    };

    return listData;
  },
};

export default listProductSettings;
