import { FindManyOptions } from 'typeorm';
import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { Product } from 'app/entity/Product';
import { ProductListOTC, ProductFilterITC } from 'app/schema/types';

const listProducts: FieldConfig<{ page: number; perPage: number; filter?: Product }> = {
  type: ProductListOTC,
  args: {
    page: {
      type: 'Int',
      defaultValue: 1,
    },
    perPage: {
      type: 'Int',
      defaultValue: 100,
    },
    filter: ProductFilterITC,
  },
  resolve: async (_, args, __, info) => {
    const productRepository = getRepository<Product>(Product);

    if (!productRepository) {
      throw new Error('DB connection error!');
    }

    const { page, perPage, filter } = args;

    const productOptions: Product = filter ? JSON.parse(JSON.stringify(filter)) : null;

    const relations = getRelationsForField(info, 'items');

    const findOptions: FindManyOptions<Product> = {
      relations,
      take: perPage,
      skip: (page - 1) * perPage,
    };

    if (productOptions) {
      findOptions.where = productOptions;
    }

    const result = await productRepository.findAndCount(findOptions);

    const listData = {
      page,
      perPage,
      items: result[0] || [],
      count: result[1] || 0,
    };

    return listData;
  },
};

export default listProducts;
