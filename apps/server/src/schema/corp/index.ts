import { directoryToAst, astToSchema } from 'graphql-compose-modules';
import { schemaComposer } from 'app/schema/graphql-compose';

function loadSchemaComposer(module: NodeModule) {
  const ast = directoryToAst(module);
  const sc = astToSchema(ast, { schemaComposer, prefix: 'Corp' });
  return sc;
}

export const buildSchema = () => loadSchemaComposer(module).buildSchema();

export { prepareContext } from './context';
