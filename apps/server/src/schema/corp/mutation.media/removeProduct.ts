import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { Product } from 'app/entity/Product';
import { ProductFilterITC, ProductOTC } from 'app/schema/types';

const removeProduct: FieldConfig<{ filter: Product }> = {
  type: ProductOTC,
  args: {
    filter: ProductFilterITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<Product>(Product);

    if (!repository) throw new Error('DB connection error!');

    const filterData: Product = JSON.parse(JSON.stringify(args.filter));

    const relations = getRelationsForField(info);

    const requiredRelations = ['settings'];

    requiredRelations.forEach((el) => {
      if (!relations.includes(el)) {
        relations.push(el);
      }
    });

    const data = await repository.findOne({
      where: filterData,
      relations,
    });
    const dataId = data?.id;

    if (!data) throw new Error('Product not found!');

    const mustEmpltyFields: string[] = [];

    requiredRelations.forEach((el) => {
      if (Array.isArray(data[el]) && data[el].length) {
        mustEmpltyFields.push(el);
      }
    });

    if (mustEmpltyFields.length) {
      throw new Error(`Product have [${mustEmpltyFields.join(', ')}] elements, remove them first!`);
    }

    await repository.remove(data);

    data.id = dataId || 0;

    return data;
  },
};

export default removeProduct;
