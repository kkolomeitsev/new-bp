import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { Product } from 'app/entity/Product';
import { ProductFilterITC, ProductUpdateITC, ProductOTC } from 'app/schema/types';

const updateProduct: FieldConfig<{ filter: Product; input: Product }> = {
  type: ProductOTC,
  args: {
    filter: ProductFilterITC.getTypeNonNull(),
    input: ProductUpdateITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<Product>(Product);

    if (!repository) {
      throw new Error('DB connection error!');
    }

    const filterData: Product = JSON.parse(JSON.stringify(args.filter));

    const data = await repository.findOne({ where: filterData });

    if (!data) throw new Error('Product not found!');

    const inputData: Product = JSON.parse(JSON.stringify(args.input));

    for (const prop in inputData) {
      data[prop] = inputData[prop];
    }

    await repository.save(data);

    const relations = getRelationsForField(info);

    const result = await repository.findOne(data.id, {
      relations,
    });

    return result;
  },
};

export default updateProduct;
