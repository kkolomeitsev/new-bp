import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { ProductSetting } from 'app/entity/ProductSetting';
import { ProductSettingFilterITC, ProductSettingOTC } from 'app/schema/types';

const removeProductSetting: FieldConfig<{ filter: ProductSetting }> = {
  type: ProductSettingOTC,
  args: {
    filter: ProductSettingFilterITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<ProductSetting>(ProductSetting);

    if (!repository) throw new Error('DB connection error!');

    const filterData: ProductSetting = JSON.parse(JSON.stringify(args.filter));

    const relations = getRelationsForField(info);

    const updateServiceRelations = ['product'];

    updateServiceRelations.forEach((el) => {
      if (!relations.includes(el)) {
        relations.push(el);
      }
    });

    const data = await repository.findOne({ where: filterData, relations });
    const dataId = data?.id;

    if (!data) throw new Error('Product setting not found!');

    await repository.remove(data);

    data.id = dataId || 0;

    return data;
  },
};

export default removeProductSetting;
