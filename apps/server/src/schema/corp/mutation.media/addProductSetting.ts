import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { ProductSetting } from 'app/entity/ProductSetting';
import { Product } from 'app/entity/Product';
import { ProductSettingITC, ProductSettingOTC } from 'app/schema/types';

const addProductSetting: FieldConfig<{ input: ProductSetting }> = {
  type: ProductSettingOTC,
  args: {
    input: ProductSettingITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<ProductSetting>(ProductSetting);
    const repositoryProduct = getRepository<Product>(Product);

    if (!(repository && repositoryProduct)) throw new Error('DB connection error!');

    const productSettingOptions: ProductSetting = JSON.parse(JSON.stringify(args.input));

    if (!productSettingOptions?.product) {
      throw new Error('Setting kind is a required field!');
    }

    if (!productSettingOptions.product.id) {
      const product = await repositoryProduct.findOne({
        where: productSettingOptions.product,
      });
      if (!product) throw new Error('Wrong product!');
      productSettingOptions.product = product;
    }

    const newProductSetting = new ProductSetting();

    for (const prop in productSettingOptions) {
      newProductSetting[prop] = productSettingOptions[prop];
    }

    const savedProductSetting = await repository.save(newProductSetting);

    const relations = getRelationsForField(info);

    const result = await repository.findOne(savedProductSetting.id, {
      relations,
    });

    return result;
  },
};

export default addProductSetting;
