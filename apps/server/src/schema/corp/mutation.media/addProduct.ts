import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { Product } from 'app/entity/Product';
import { ProductSetting } from 'app/entity/ProductSetting';
import { ProductITC, ProductOTC } from 'app/schema/types';

const addProduct: FieldConfig<{ input: Product }> = {
  type: ProductOTC,
  args: {
    input: ProductITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<Product>(Product);
    const repositoryProductSetting = getRepository<ProductSetting>(ProductSetting);

    if (!(repository && repositoryProductSetting)) {
      throw new Error('DB connection error!');
    }

    const productOptions: Product = JSON.parse(JSON.stringify(args.input));

    const newProduct = new Product();

    for (const prop in productOptions) {
      newProduct[prop] = productOptions[prop];
    }

    const savedProduct = await repository.save(newProduct);

    const newProductSetting = new ProductSetting();
    newProductSetting.product = savedProduct;
    newProductSetting.title = 'serviceName';
    newProductSetting.description = 'Required setting!';
    newProductSetting.value = '';
    await repositoryProductSetting.save(newProductSetting);

    const relations = getRelationsForField(info);

    const result = await repository.findOne(savedProduct.id, {
      relations,
    });

    return result;
  },
};

export default addProduct;
