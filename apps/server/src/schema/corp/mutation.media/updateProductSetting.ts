import { FieldConfig } from 'app/schema/graphql-compose';
import { getRepository } from 'app/utils/db';
import { getRelationsForField } from 'app/utils/getRelationsForField';
import { ProductSetting } from 'app/entity/ProductSetting';
import { Product } from 'app/entity/Product';
import {
  ProductSettingFilterITC,
  ProductSettingOTC,
  ProductSettingUpdateITC,
} from 'app/schema/types';

const updateProductSetting: FieldConfig<{ filter: ProductSetting; input: ProductSetting }> = {
  type: ProductSettingOTC,
  args: {
    filter: ProductSettingFilterITC.getTypeNonNull(),
    input: ProductSettingUpdateITC.getTypeNonNull(),
  },
  resolve: async (_, args, __, info) => {
    const repository = getRepository<ProductSetting>(ProductSetting);
    const repositoryProduct = getRepository<Product>(Product);

    if (!(repository && repositoryProduct)) throw new Error('DB connection error!');

    const filterData: ProductSetting = JSON.parse(JSON.stringify(args.filter));

    const data = await repository.findOne({
      where: filterData,
      relations: ['product'],
    });

    if (!data) throw new Error('Product setting not found!');

    const inputData: ProductSetting = JSON.parse(JSON.stringify(args.input));

    if (inputData?.product) {
      if (!inputData.product?.id) {
        const product = await repositoryProduct.findOne({
          where: inputData.product,
        });
        if (!product) throw new Error('Wrong product!');
        inputData.product = product;
      }
    }

    for (const prop in inputData) {
      data[prop] = inputData[prop];
    }

    await repository.save(data);

    const relations = getRelationsForField(info);

    const result = await repository.findOne(data.id, {
      relations,
    });

    return result;
  },
};

export default updateProductSetting;
