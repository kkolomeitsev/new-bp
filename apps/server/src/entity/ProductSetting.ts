import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Product } from './Product';

@Entity({ name: 'product_settings' })
export class ProductSetting {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true, type: 'text' })
  description?: string;

  @Column({ default: 'StringValue' })
  kindType: string;

  @Column()
  value: string;

  @ManyToOne(
    (_type) => Product,
    (product) => product.settings
  )
  product?: Product;
}
