import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ProductSetting } from './ProductSetting';

@Entity({ name: 'products' })
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  title: string;

  @Column({ nullable: true, type: 'text' })
  description?: string;

  @OneToMany(
    (_type) => ProductSetting,
    (setting) => setting.product
  )
  settings?: ProductSetting[];
}
