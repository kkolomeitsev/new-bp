import { TContext } from 'app/schema/corp/context'; // TODO: fix this: should corp and public

export interface ResolverConfig {
  source: any;
  args: any;
  context: TContext;
  info: any;
}
