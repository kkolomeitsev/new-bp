declare namespace Express {
  export interface Response {
    sendHttpError: (err: any) => void;
  }
}
