// Переопределение класса ошибки для удобства
import http from 'http';
import express from 'express';

export class HttpError extends Error {
  status: number;
  constructor(status: number, message?: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
    this.status = status;
    this.message = message || http.STATUS_CODES[status] || 'Unknown error';
  }
}

HttpError.prototype.name = 'HttpError';

export const errorCatcher = (app: express.Application) => {
  app.use((req, res) => {
    const err = new HttpError(404, 'PAGE NOT FOUND');
    res.sendHttpError(err);
  });

  app.use(
    (
      err: number | HttpError | Error,
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      if (typeof err === 'number') {
        err = new HttpError(err);
      } else {
        err = new HttpError(500, err.message);
      }
      res.sendHttpError(err);
    }
  );
};
