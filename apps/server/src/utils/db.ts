import path from 'path';
import { ENV } from 'app/configs';
import { createConnection, Connection } from 'typeorm';

interface DbObject {
  connection?: Connection;
}

export const db: DbObject = {};

export async function connect() {
  let connection;
  try {
    connection = await createConnection({
      type: 'postgres',
      host: ENV.POSTGRES_HOST,
      port: ENV.POSTGRES_PORT,
      username: ENV.POSTGRES_USERNAME,
      password: ENV.POSTGRES_PASSWORD,
      database: ENV.POSTGRES_DATABASE,
      synchronize: ENV.POSTGRES_SYNCHRONIZE,
      logging: ENV.POSTGRES_LOGGING,
      entities: [path.resolve(__dirname, '../entity/*.js')],
      migrations: [path.resolve(__dirname, '../migration/*.js')],
      subscribers: [path.resolve(__dirname, '../subscriber/*.js')],
    });
    db.connection = connection;
  } catch (e) {
    console.log('TypeORM connection fall, error:', e);
  }
  return connection;
}

export function getRepository<T>(Entity: any) {
  return db.connection?.getRepository<T>(Entity);
}

export function getTreeRepository<T>(Entity: any) {
  return db.connection?.getTreeRepository<T>(Entity);
}
