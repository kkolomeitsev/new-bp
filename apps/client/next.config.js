/* eslint-disable */
const path = require('path');
const withFonts = require('next-fonts');
const withReactSvg = require('next-react-svg');
const withImages = require('next-images');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');

if (typeof require !== 'undefined') {
  require.extensions['.css'] = (file) => {};
}

const config = withImages(
  withReactSvg(
    withFonts({
      cssModules: false,
      distDir: '../dist',
      imageTypes: ['jpg', 'png'],
      inlineImageLimit: 16384,
      enableSvg: true,
      include: path.resolve(__dirname, 'src/assets/svg'),
    })
  )
);

module.exports = config;
