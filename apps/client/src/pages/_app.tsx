import React from 'react';
import NProgress from 'nprogress';
import App from 'next/app';
import Head from 'next/head';
import Router from 'next/router';

import 'antd/dist/antd.css';
import 'react-quill/dist/quill.snow.css';
import 'app/assets/style/custom.scss';

Router.events.on('routeChangeStart', () => {
  NProgress.start();
});
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <link rel="shortcut icon" type="image/x-icon" href="/static/img/favicon.ico" />
        </Head>
        <Component {...pageProps} />
      </React.Fragment>
    );
  }
}
