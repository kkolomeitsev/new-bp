import React from 'react';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import Test from 'app/components/Test/Test';
import { withApollo } from 'app/lib/withApollo';

function Index() {
  return (
    <RootWrapper>
      <div className="content">
        <h2>Главная</h2>
        <Test />
      </div>
    </RootWrapper>
  );
}

export default withApollo(Index);
