import React, { useState, useEffect, FunctionComponent, ComponentProps } from 'react';
import { useRouter } from 'next/router';
import { Layout } from 'antd';
import SidebarMenu from './SidebarMenu';
import logoSrc from 'app/assets/img/logo.png';

const { Header, Content, Sider } = Layout;

function RootWrapper(props: ComponentProps<FunctionComponent>) {
  const { children } = props;

  const [init, setInit] = useState(false);

  useEffect(() => {
    if (init) {
      const preloader = document ? document.getElementById('preloader') : null;
      if (preloader) {
        preloader.style.opacity = '0';
        setTimeout(() => {
          preloader.style.display = 'none';
        }, 700);
      }
    }
  }, [init]);

  if (!init && typeof window !== 'undefined') {
    setInit(true);
  }

  const router = useRouter();

  return (
    <div className="body ant-layout" style={{ minHeight: '100vh' }}>
      <div id="preloader" className="preloader" />
      <Header className="header" style={{ padding: '0 20px', backgroundColor: '#ffffff' }}>
        <img
          className="logo"
          style={{ display: 'block', marginTop: '7px', maxHeight: '50px', cursor: 'pointer' }}
          src={logoSrc}
          onClick={() => {
            router.replace('/');
            return;
          }}
        />
      </Header>
      <Layout>
        <Sider className="sider" width={256}>
          <SidebarMenu />
        </Sider>
        <div className="ant-layout right-side">
          <Content className="content-wrapper">{children}</Content>
        </div>
      </Layout>
    </div>
  );
}

export default RootWrapper;
