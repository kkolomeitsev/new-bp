/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Menu } from 'antd';
import { AppstoreOutlined } from '@ant-design/icons';
import { withRouter, SingletonRouter } from 'next/router';

interface P {
  router: SingletonRouter;
}

function sidebarMenu(props: P) {
  return (
    <Menu theme="dark" mode="inline" selectedKeys={[props.router!.pathname]}>
      <Menu.Item key="/" onClick={() => props.router!.push('/')}>
        <AppstoreOutlined />
        <span className="nav-text">Главная</span>
      </Menu.Item>
    </Menu>
  );
}

export default withRouter(sidebarMenu);
