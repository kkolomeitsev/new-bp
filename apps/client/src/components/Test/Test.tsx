import React from 'react';
import { Alert, Spin } from 'antd';
import { useTestQuery } from './__generated__/TestQuery';

export default function Test() {
  const { data, loading, error } = useTestQuery();

  if (error) {
    return (
      <Alert
        message="Ошибка"
        description={error?.message ? error.message : error}
        type="error"
        closable
      />
    );
  }

  return <Spin spinning={loading}>{JSON.stringify(data, null, 2)}</Spin>;
}
