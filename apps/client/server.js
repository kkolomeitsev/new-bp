/* eslint-disable @typescript-eslint/no-var-requires */
require.extensions['.css'] = (_file) => {};

require('dotenv').config({
  path: require('path').resolve(process.cwd(), '.env.dev'),
  debug: !!process.env.DEBUG,
});

require('module-alias').addAlias('app', __dirname);
const express = require('express');
const next = require('next');
global.fetch = require('isomorphic-unfetch');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const cfg = {
  dev,
  dir: dev ? './src' : './',
  conf: {
    distDir: './dist',
    poweredByHeader: false,
    publicRuntimeConfig: {
      APP_GRAPHQL_URI: process.env.APP_GRAPHQL_URI,
      ENV_TYPE: process.env.ENV_TYPE,
    },
    ...(dev ? require('./next.config.js') : null),
  },
};
const app = next(cfg);
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  if (!dev) {
    server.enable('trust proxy');
  }

  server.disable('X-Powered-By');

  server.use(function(req, res, next) {
    res.removeHeader('X-Powered-By');
    next();
  });

  server.use('/static', express.static('static', { etag: true }));

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`🚨 🚀 Client SSR app ready at http://localhost:${port}`);
  });
});
